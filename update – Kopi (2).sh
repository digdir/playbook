#!/bin/bash
echo "Updating component architecture-repository..."
echo "=========================="
cd /c/gitlab/digdir/components/architecture-repository
git checkout master
git add .
git commit -m "Dev"
git push origin master
echo ""
echo "Updating platform modules..."
echo "=============================="
cd /c/gitlab/digdir/components/platform-components/platform
git checkout master
git add .
git commit -m "Dev"
git push origin master
echo ""
echo "Updating knowledge modules..."
echo "==============================="
cd /c/gitlab/digdir/components/knowledge-components/knowledge
git checkout master
git add .
git commit -m "Dev"
git push origin master
echo ""
echo ""
echo "Updating drafts modules..."
echo "==============================="
cd /c/gitlab/digdir/components/draft-components/drafts
git checkout master
git add .
git commit -m "Dev"
git push origin master
echo ""
echo ""
echo "Updating playbook..."
echo "===================="
cd /c/gitlab/digdir/playbook
git checkout master
git add .
git commit -m "Dev"
git push origin master
echo ""
echo ""
echo "Running Antora..."
antora --fetch antora-playbook.yml --attribute lang=no 	--attribute wysiwig_editing=0 --attribute target_audience=all --attribute include_early_drafts=2
echo "Updating site..."
cd /c/gitlab/digdir/output/github-pages/nasjonal-arkitektur.github.io
touch .nojekyll
#git checkout master
#git add .
#git commit -m "Dev"
#git push origin master
