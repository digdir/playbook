#!/bin/bash
cd /c
mkdir /c/gitlab
cd /c/gitlab
mkdir /c/gitlab/digdir
cd /c/gitlab/digdir
git clone https://gitlab.com/digdir/playbook
git clone https://gitlab.com/digdir/antora-ui
git clone https://gitlab.com/digdir/docs
mkdir /c/gitlab/digdir/components
echo ""
echo "Cloning architecture-repository..."
echo "=============================="
cd /c/gitlab/digdir/components
git clone https://gitlab.com/digdir/components/architecture-repository
echo ""
echo "Cloning platform modules..."
echo "==========================="
mkdir /c/gitlab/digdir/components/platform-components
cd /c/gitlab/digdir/components/platform-components
git clone https://gitlab.com/digdir/components/platform-components/platform
echo ""
echo "Cloning knowledge modules..."
echo "============================"
mkdir /c/gitlab/digdir/components/knowledge-components
cd /c/gitlab/digdir/components/knowledge-components
git clone https://gitlab.com/digdir/components/knowledge-components/knowledge
echo ""
echo "Cloning drafts modules..."
echo "========================="
mkdir /c/gitlab/digdir/components/draft-components/
cd /c/gitlab/digdir/components/draft-components/
git clone https://gitlab.com/digdir/components/draft-components/drafts
echo ""
https://github.com/nasjonal-arkitektur/nasjonal-arkitektur.github.io
echo "Cloning output repos"
echo "========================="
mkdir /c/gitlab/digdir/output/
mkdir /c/gitlab/digdir/output/github-pages/
cd /c/gitlab/digdir/output/github-pages/
git clone https://github.com/nasjonal-arkitektur/nasjonal-arkitektur.github.io
echo ""
echo "Done"