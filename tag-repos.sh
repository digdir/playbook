#!/bin/bash
cd /c/gitlab/digdir/playbook
git tag -a v2.1 -m "playbook v2.1"
git push origin --tags
git tag

# How to delete a tag:
#git fetch
#git tag -d v2.1
#git push origin :refs/tags/v2.1
#git tag



cd /c/gitlab/digdir/antora-ui
git tag -a v2.1 -m "antora-ui v2.1"
git push origin --tags
git tag


cd /c/gitlab/digdir/components/architecture-repository
git tag -a v2.1 -m "architecture-repository v2.1"
git push origin --tags
git tag


cd /c/gitlab/digdir/components/platform-components/platform
git tag -a v2.1 -m "platform v2.1"
git push origin --tags
git tag


cd /c/gitlab/digdir/components/knowledge-components/knowledge
git tag -a v2.1 -m "knowledge v2.1"
git push origin --tags
git tag


cd /c/gitlab/digdir/components/draft-components/drafts
git tag -a v2.1 -m "drafts v2.1"
git push origin --tags
git tag


cd /c/gitlab/digdir/output/github-pages/nasjonal-arkitektur.github.io
git tag -a v2.1 -m "nasjonal-arkitektur.github.io v2.1"
git push origin --tags
git tag
