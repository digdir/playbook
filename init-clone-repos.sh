#!/bin/bash
cd /c/Users/eha/OneDrive/
mkdir /c/Users/eha/OneDrive/antora
cd /c/Users/eha/OneDrive/antora
mkdir /c/Users/eha/OneDrive/antora/digdir
cd /c/Users/eha/OneDrive/antora/digdir
git clone https://gitlab.com/digdir/playbook
git clone https://gitlab.com/digdir/antora-ui
mkdir /c/Users/eha/OneDrive/antora/digdir/components
echo ""
echo "Cloning architecture-repository..."
echo "=============================="
cd /c/Users/eha/OneDrive/antora/digdir/components
git clone https://gitlab.com/digdir/components/architecture-repository
echo ""
echo "Cloning platform modules..."
echo "==========================="
mkdir /c/Users/eha/OneDrive/antora/digdir/components/platform-components
cd /c/Users/eha/OneDrive/antora/digdir/components/platform-components
git clone https://gitlab.com/digdir/components/platform-components/platform
echo ""
echo "Cloning knowledge modules..."
echo "============================"
mkdir /c/Users/eha/OneDrive/antora/digdir/components/knowledge-components
cd /c/Users/eha/OneDrive/antora/digdir/components/knowledge-components
git clone https://gitlab.com/digdir/components/knowledge-components/knowledge
echo ""
echo "Cloning drafts modules..."
echo "========================="
mkdir /c/Users/eha/OneDrive/antora/digdir/components/draft-components/
cd /c/Users/eha/OneDrive/antora/digdir/components/draft-components/
git clone https://gitlab.com/digdir/components/draft-components/drafts
echo ""
https://github.com/nasjonal-arkitektur/nasjonal-arkitektur.github.io
echo "Cloning output repos"
echo "========================="
mkdir /c/Users/eha/OneDrive/antora/digdir/output/
mkdir /c/Users/eha/OneDrive/antora/digdir/output/github-pages/
cd /c/Users/eha/OneDrive/antora/digdir/output/github-pages/
git clone https://github.com/nasjonal-arkitektur/nasjonal-arkitektur.github.io
echo ""
echo "Done"