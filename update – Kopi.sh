#!/bin/bash
echo "Updating component architecture-repository..."
echo "=========================="
cd /c/gitlab/digdir/components/architecture-repository
git checkout master
git add .
git commit -m "Dev"
git push origin master
echo ""
echo "Updating platform modules..."
echo "=============================="
cd /c/gitlab/digdir/components/platform-components/platform
git checkout master
git add .
git commit -m "Dev"
git push origin master
echo ""
echo "Updating knowledge modules..."
echo "==============================="
cd /c/gitlab/digdir/components/knowledge-components/knowledge
git checkout master
git add .
git commit -m "Dev"
git push origin master
echo ""
echo "Updating component playbook..."
cd /c/gitlab/digdir/playbook
git checkout master
git add .
git commit -m "Dev"
git push origin master
echo "Running Antora..."
antora --fetch antora-playbook.yml --attribute lang=no wysiwig_editing=0
echo ""
#echo "NOT Updating component site..."
cd /c/gitlab/digdir/docs
git checkout master
git add .
git commit -m "Dev"
git push origin master
